#AngularPoC

##Configuration

The local configuration required:

1. Open IIS Manager
2. Create a new site for AngularPoC.WebAPI project:
	1. Site name: ngwebapi
	2. Physical path: (source root)/AngularPoC/AngularPoC.WebAPI
	3. Host name: ngwebapi
3. Create a new site for AngularPoC.Web project:
	1. Site name: ngweb
	2. Physical path: (source root)/AngularPoC/AngularPoC.Web
	3. Host name: ngweb
4. Open hosts file (c:\Windows\System32\drivers\etc\hosts)
5. Add the following localhost entries:
	1. ngweb
	2. ngwebapi

##Execution

To run this project:

1. Open solution and compile
2. Navigate to http://ngweb/

##Example URLs

Web: 

1. User List: http://ngweb/
2. User Detail: http://ngweb/detail/ffc9ff59-71b6-418e-b254-c90f7cb8242e

WebAPI:

1. Get all users: http://ngwebapi/api/users/
2. Get a user: http://ngwebapi/api/users/ffc9ff59-71b6-418e-b254-c90f7cb8242e
