﻿using AngularPoC.WebAPI.Controllers;
using AngularPoC.WebAPI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularPoC.WebAPI.Tests.Controllers
{
    [TestClass]
    public class UsersControllerTest
    {
        [TestMethod]
        public async Task Get()
        {
            // Arrange
            var controller = new UsersController();

            // Act
            var users = await controller.Get();

            // Assert
            Assert.IsNotNull(users);
            Assert.AreEqual(3, users.Count());
        }

        [TestMethod]
        public async Task GetById()
        {
            // Arrange
            var controller = new UsersController();
            var id = Guid.Parse("ffc9ff59-71b6-418e-b254-c90f7cb8242e");

            // Act
            var user = await controller.Get(id);

            // Assert
            Assert.IsNotNull(user);
            Assert.AreEqual(id, user.Id);
        }

        //[TestMethod]
        //public async Task Post()
        //{
        //    // Arrange
        //    var controller = new UsersController();

        //    // Act
        //    //controller.Post("value");

        //    // Assert
        //}

        //[TestMethod]
        //public async Task Put()
        //{
        //    // Arrange
        //    var controller = new UsersController();

        //    // Act
        //    //controller.Put(5, "value");

        //    // Assert
        //}

        //[TestMethod]
        //public async Task Delete()
        //{
        //    // Arrange
        //    var controller = new UsersController();
        //    var id = Guid.Parse("ffc9ff59-71b6-418e-b254-c90f7cb8242e");

        //    // Act
        //    controller.Delete(id);

        //    // Assert
        //}
    }
}
