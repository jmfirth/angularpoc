﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace AngularPoC.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            // retrieve configuration-specific WebApiUrl
            ViewBag.WebApiUrl = WebConfigurationManager.AppSettings["WebApiUrl"];
            return View();
        }
    }
}