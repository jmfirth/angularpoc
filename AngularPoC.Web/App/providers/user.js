﻿angular
    .module('app')
    .factory('User', [
        '$resource',
        function ($resource) {
            return $resource(webApiUrl + 'users/:id');
        }
    ]);

//angular
//    .module('app')
//    .provider('User', [
//        '$',
//        function ($) {
//            console.log('User provider');

//            var self = this;
//            var initInjector = angular.injector(['ng']);
//            var $http = initInjector.get('$http');
//            //console.log($http);
//            //var $scope = initInjector.get('$scope');
//            //console.log($scope);
//            var $rootScope = initInjector.get('$rootScope');
//            //console.log($rootScope);

//            this.$get = [function ($injector) {
//                return {
//                    getUsers: function() {
//                        var request = $http({
//                            method: 'get',
//                            url: 'http://ngwebapi/api/users'
//                        });
//                        return request.then(function (res) {
//                            console.log('User.getUsers success callback');
//                            console.log(res);
//                            $rootScope.$emit('usersFetched', res.data);
//                            $rootScope.$broadcast('usersFetched', res.data);
//                        }, function (res) {
//                            console.log('User.getUsers error callback');
//                            console.log(res);
//                        });
//                    },
//                    getUser: function(userId) {
//                        var request = $http({
//                            method: 'get',
//                            url: 'http://ngwebapi/api/users/' + userId,
//                        });
//                        return request.then(function (res) {
//                            console.log('User.getUser(userId) success callback');
//                            console.log(res);
//                        }, function (res) {
//                            console.log('User.getUser(userId) error callback');
//                            console.log(res);
//                        });
//                    }
//                };
//            }];
//        }
//    ]);