﻿var app = angular.module('app', ['ui.router', 'ngResource']);
app.constant('$', $);

function initRoutes($locationProvider, $stateProvider, $urlRouterProvider, $httpProvider) {
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/");

    //Rewrite the url without hashes and use the history API
    $locationProvider.html5Mode(true);

    $stateProvider
        .state('wrapper', {
            templateUrl: '/App/views/wrapper.html',
            controller: 'Wrapper'
        })
        .state('user-wrapper', {
            templateUrl: '/App/views/user-wrapper.html',
            controller: 'UserWrapper',
            parent: 'wrapper'
        })
        .state('list', {
            url: '/',
            templateUrl: '/App/views/user-list.html',
            controller: 'UserList',
            parent: 'user-wrapper'
        })
        .state('detail', {
            url: '/detail/:id',
            templateUrl: '/App/views/user-detail.html',
            controller: 'UserDetail',
            parent: 'user-wrapper'
        });
};
initRoutes.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider'];

app.config(initRoutes);