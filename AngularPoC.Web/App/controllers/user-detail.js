﻿angular
    .module('app')
    .controller('UserDetail', [
        '$scope', '$state', '$stateParams', '$timeout', 'User',
        function ($scope, $state, $stateParams, $timeout, User) {
            console.log('User Detail controller loading...');

            // view model
            $scope.user = null;

            // get an individual user
            User.get({ id: $stateParams.id }, function(user) {
                $scope.user = user;
                $timeout(function() {
                    $scope.$apply();
                });
            });
        }
    ]);