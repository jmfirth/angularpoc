﻿angular
    .module('app')
    .controller('UserList', [
        '$scope', '$state', '$timeout', 'User',
        function ($scope, $state, $timeout, User) {
            console.log('User List controller loading...');

            // view model
            $scope.users = [];

            // get all users
            User.query(function(users) {
                $scope.users = users;
                $timeout(function () {
                    $scope.$apply();
                });
            });
        }
    ]);