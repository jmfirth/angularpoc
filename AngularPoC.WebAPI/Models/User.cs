﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularPoC.WebAPI.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public DateTime CreateDate { get; set; }
    }
}