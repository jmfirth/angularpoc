﻿using System.Threading.Tasks;
using AngularPoC.WebAPI.Filters;
using AngularPoC.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AngularPoC.WebAPI.Controllers
{
    [AllowCrossSiteJson]
    public class UsersController : ApiController
    {
        private List<User> Users { get; set; } 

        public UsersController()
        {
            // seed WebAPI controller
            Users = new List<User>()
            {
                new User()
                {
                    Id = Guid.Parse("ffc9ff59-71b6-418e-b254-c90f7cb8242e"),
                    Username = "user1",
                    PasswordHash = "",
                    CreateDate = DateTime.Parse("2012-01-01")
                },
                new User()
                {
                    Id = Guid.Parse("514580e1-c697-4b7b-990c-a7d7b305ec9d"),
                    Username = "user2",
                    PasswordHash = "",
                    CreateDate = DateTime.Parse("2013-01-01")
                },
                new User()
                {
                    Id = Guid.Parse("b788ea48-23f4-42e0-be73-69e0b0a4c138"),
                    Username = "user3",
                    PasswordHash = "",
                    CreateDate = DateTime.Parse("2014-01-01")
                },
            };
        }

        // GET api/user
        public async Task<IEnumerable<User>> Get()
        {
            return await Task.Run<IEnumerable<User>>(() => Users);
        }

        // GET api/users/<Guid>
        public async Task<User> Get(Guid id)
        {
            return await Task.Run<User>(() =>
            {
                var user = Users.Find(u => u.Id == id);
                if (user != null)
                    return user;
                return null;
            });
        }

        //// POST api/user
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/users/5
        //public void Put(int Guid, [FromBody]string value)
        //{
        //}

        //// DELETE api/users/5
        //public void Delete(int id)
        //{
        //}
    }
}
